Independent documentaries, created to interest and challenge perceptions of the viewers. All with close to zero budget and on a very tight time budget.

Website: https://spectism.com/
